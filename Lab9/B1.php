<?php
    class Employee
    {
        private $id;
        private $name;
        private $workingHoursPerDay = 8;
        private $hourlyRate = 15;
        private $totalLeaveTaken;
        private $workingDays;
        private $totalDays;

        public function __construct($id, $name, $totalLeaveTaken, $workingDays)
        {
            $this->id = $id;
            $this->name = $name;
            $this->totalLeaveTaken = $totalLeaveTaken;
            $this->workingDays = $workingDays;
            $this->totalDays = $this->workingDays - $this->totalLeaveTaken;
        }
        public function getSalaryAmount($totalDays)
        {
            $salary = ($this->workingDays - $this->totalLeaveTaken) * $this->workingHoursPerDay * $this->hourlyRate;
            return $salary;
        }
        public function getId()
        {
            return $this->id;
        }
        public function getName()
        {
            return $this->name;
        }
        public function getWorkingHoursPerDay()
        {
            return $this->workingHoursPerDay;
        }
        public function getHourlyRate()
        {
            return $this->hourlyRate;
        }
        public function getTotalLeaveTaken()
        {
            return $this->totalLeaveTaken;
        }
        public function getWorkingDays()
        {
            return $this->workingDays;
        }
        public function getTotalDays()
        {
            return $this->totalDays;
        }
        public function printInfo()
        {
            echo "{$this->name} has worked for {$this->workingDays} and taken {$this->totalLeaveTaken} leaves. {$this->name} salary is {$this->getSalaryAmount($this->totalDays)}";
        }
    }
    $employee1 = new Employee(1, "John, Smith", 4, 16);
    $employee2 = new Employee(2, "Jacob", 2, 18);
    $employee1->printInfo();
    echo "<br>";
    $employee2->printInfo();
    echo "<br>";
?>