<?php
    abstract class Database {
        private $connection;
        private $connectionActive = false;
        abstract public function setConnection($dbName);
        abstract public function getConnection();
    }

    class DBConnection extends Database {
        public function setConnection($dbName) {
            $this->connectionActive = true;
        }

        public function getConnection() {
            if ($this->connectionActive) {
                echo "Connected to database";
            }
        }
    }

    $db = new DBConnection();
    $db->setConnection("MySQL");
    $db->getConnection();
?>