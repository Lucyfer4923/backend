<?php
    class Animal {
        private $name;
        public $hunt;
        public function __construct($name, $hunt) {
            $this->name = $name;
            $this->hunt = $hunt;
        }
    }

    class Dog extends Animal {
        public function makeSound() {
            echo "Woff, woff";
        }
    }
    class Tiger extends Animal {
        public function makeSound() {
            echo "Grrrr, grrrr";
        }
    }

    $dog = new Dog("Dog", true);
    $tiger = new Tiger("Tiger", true);
    echo $dog->makeSound();
    echo "<br>";
    echo $tiger->makeSound();
    echo "<br>";
    echo $dog->hunt;
    echo "<br>";
    echo $tiger->hunt;


?>