<?php
    class Bank {
        private $accNO;
        private $name;
        private $balance;

        public function depositAmount($amount) {
            $this->balance += $amount;
        }

        public function deductAmount($amount) {
            if ($this->balance < 0) {
                echo "No balance in account";
            } else if ($this->balance < $amount) {
                echo "Requested amount is greater than balance";
            } else {
                $this->balance -= $amount;
            }
        }

        public function checkBalance() {
            return $this->balance;
        }

        public function __construct($accNO, $name, $balance) {
            $this->accNO = $accNO;
            $this->name = $name;
            $this->balance = $balance;
        }
    }

    $bank1 = new Bank(1, "SBI", 4000);
    $bank1->depositAmount(1000);
    $bank1->deductAmount(500);
    echo $bank1->checkBalance();
?>