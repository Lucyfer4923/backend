<?php
    interface Phone {
        public function makeCall($number);
        public function sendMessage($number, $message);
    }

    class iOS implements Phone {
        public function makeCall($number) {
            echo "Calling $number on iOS";
        }

        public function sendMessage($number, $message) {
            echo "Sending message $message to $number on iOS";
        }
    }

    $ios = new iOS();
    $ios->makeCall("09876");
    echo "<br>";
    $ios->sendMessage("09876", "Hello");
?>