<?php
    /*
        Khởi tạo một class Bank có các thuộc tính như sau:
        accNo
        name
        balance (Tiền trong tài khoản)
     */
    class Bank {
        private $accNO;
        private $name;
        private $balance;

        /*
            Viết 2 hàm depositAmount($amount) để thêm $amount vào balance của tài khoản và viết 1 hàm deductAmount($amount) để rút tiền từ tài khoản. Kiểm tra 3 điều kiện sau
            Nếu balance < 0 thì in ra màn hình “No balance in account”
            Nếu balance < $amount thì in ra màn hình “Requested amount is greater than balance”
            Ngược lại thì balance mới = balance cũ – amount.
        */

        public function depositAmount($amount) {
            $this->balance += $amount;
        }

        public function deductAmount($amount) {
            if ($this->balance < 0) {
                echo "No balance in account";
            } else if ($this->balance < $amount) {
                echo "Requested amount is greater than balance";
            } else {
                $this->balance -= $amount;
            }
        }

        public function checkBalance() {
            return $this->balance;
        }

        /*
        Khởi tạo instance của object và sử dụng depositAmount()và deductAmount() và in ra balance của object instance đó.

        */

        public function __construct($accNO, $name, $balance) {
            $this->accNO = $accNO;
            $this->name = $name;
            $this->balance = $balance;
        }
    }

    $bank1 = new Bank(1, "SBI", 1000);
    $bank1->depositAmount(500);
    $bank1->deductAmount(500);
    echo $bank1->checkBalance();
    

    
    

?>